# Stress-Induced Mutagenesis and Complex Adaptation
## Yoav Ram and Lilach Hadany

This repository is the workspace for a publication of the same title, published by _Proceedings B_ in 2014.

### Citation

Ram, Y. and Hadany, L. 2014. Stress-induced mutagenesis anc complex adaptation. Proceedings of the Royal Society B, 281: 20141025. DOI: [10.1098/rspb.2014.1025](http://prsb2014.yoavram.com) | [arXiv](http://arxiv.org/abs/1407.3548) | [bioRxiv](http://dx.doi.org/10.1101/007096)

### Data citation

Ram Y., Hadany L. (2014) Data from: Stress-induced mutagenesis and complex adaptation. Dryad Digital Repository. DOI: [10.5061/dryad.3066j](http://dx.doi.org/10.5061/dryad.3066j).

### License

The content of this repository is under copyright to the authors.
However, the authors are happy to release portions of the content under a permissive license upon request and may do so within the context of this reposityory in the future.

The data used for the publication is deposited at [dryad](http://dx.doi.org/10.5061/dryad.3066j) under a CC0 license.

The licenses of the versions of the manuscript published by [Proceedings B](http://prsb2014.yoavram.com) and posted to [arXiv](http://arxiv.org/abs/1407.3548) and [bioRxiv](http://dx.doi.org/10.1101/007096) appear on the respective websites.

[SIDEER2013]: http://sideer2013.yoavram.com/
